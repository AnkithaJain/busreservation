﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BusReservation.Startup))]
namespace BusReservation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
